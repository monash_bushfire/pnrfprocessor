make_windows_from_ts <- function(x, window_size, step_size=1){
  # x is a vector
  num_times <- floor((length(x)-window_size)/step_size) +1 
  out <- list()
  st <- 1
  en <- window_size
  for( i in 1:num_times ){
    out[[i]] <- x[st:en]
    st <- st + step_size
    en <- min(en + step_size, length(x))
  }
  return(out)
}


compute_accuracy_bygroup <- function(preds,test_labels,test_file_nums){
  require("plyr")
  # df <- cbind.data.frame(preds,test_labels,test_file_nums)
  file_nums <- unique(test_file_nums)
  num_files <- length(file_nums)
  df <- data.frame(act=factor(),prs=factor())
  levels(df$prs) <- levels(df$act) <- levels(test_labels)
  #prs <- rep("P",num_files)
  #act <- rep("P",num_files)
  for(i in 1:num_files){
    inds <- which(test_file_nums %in% file_nums[i])
    df[i,1] <- getmode(test_labels[inds])
    df[i,2] <- getmode(preds[inds])
  }
  # acc1 <- sum(df[,1]==df[,2])/dim(df)[1]
  # tab <- table(df[,1], df[,2])
  # rsum <- rowSums(tab)
  # inds <- which(rsum!=0)
  # out <- tab
  # out[inds,] <- tab[inds,]/rsum[inds]
  out <- table(df[,1], df[,2])
  return(out)
}

getmode <- function(v) {
  uniqv <- unique(v)
  uniqv[which.max(tabulate(match(v, uniqv)))]
}

collate_features <- function(folder, spcs_fname, pat="csv"){
  files_list <- list.files(folder, pattern=pat)
  species_info <- read.csv(spcs_fname)
  for(i in 1:length(files_list)){
    filename <- files_list[i]
    dat <- read.csv(paste(folder, filename, sep=""))
    nn <- dim(dat)[1]
    # Get species 
    sp <- get_species_1(species_info, filename) 
    
    temp <- cbind( matrix( c(rep(filename,nn), (1:nn)), ncol=2 ), dat,  rep(sp, nn) ) 
    colnames(temp) <- c("filename", "rec_number", colnames(dat), "species")
    if(i==1){
      dat_all <- temp
    }else{
      dat_all <- rbind.data.frame(dat_all, temp)
    }
  }
  return(dat_all)
}


get_species_1 <- function(species_info, filename){
  # for file filenames_3_species_No_ignition
  # species_info contains  filenames_3_species_No_ignition
  
  filenames <- paste("DownSampled_VT", species_info[,1],".csv_features.csv", sep="")
  ind <- which(filenames %in% filename)
  species <- species_info$Species[ind]
  return(species)
}