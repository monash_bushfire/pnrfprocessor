#!/bin/env bash
#SBATCH --job-name=Data_generation-%j-%A
#SBATCH --time=5:00:00
#SBATCH --mem=16000mb
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=nandini.anantharama@monash.edu

echo "Starting on `hostname`"
echo "JOBID is  $SLURM_JOB_ID  and SLURM_ARRAY_TASK_ID is  ${SLURM_ARRAY_TASK_ID}"

# input R requires
# input_dir = args[1]
# output_dir = args[2]
# fault_file = args[3]
# k = args[4]
# etype = args[5]
# equal = args[6]

INPUT_FILE_DIR='~/uy13/DataFolder/tsfeatures/features_step_1/'
OUTPUT_FILE_DIR='~/uy13/nsana4/result/'
FAULT_FILE_LOC='~/uy13/DataFolder/Other_data/basic_run_sheet.csv'
K=5
ETYPE='seperate'
EQUAL=false

module load R/3.4.3
time R --vanilla < ~/uy13/nsana4/experiments/data_generator.R --args $INPUT_FILE_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC $K $ETYPE $EQUAL
echo "Done"
