#!/bin/env bash
#SBATCH --job-name=Ignition_detection-%j-%A
#SBATCH --time=5:00:00
#SBATCH --mem=16000mb
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=nandini.anantharama@monash.edu

echo "Starting on `hostname`"
echo "JOBID is  $SLURM_JOB_ID  and SLURM_ARRAY_TASK_ID is  ${SLURM_ARRAY_TASK_ID}"

# input_dir = args[1]
# k_fold_dir = args[2]
# output_dir = args[3]
# fault_file = args[4]
# command_arg = args[5]
# k <- args[6]
# type <- args[7]
# model <- args[8]
# cost <- args[9] # required for logistic
# fold <- args[9] # required for other models

INPUT_FILE_DIR='~/uy13/DataFolder/tsfeatures/features_step_1/'
OUTPUT_FILE_DIR='~/uy13/nsana4/result/'
FAULT_FILE_LOC='~/uy13/DataFolder/Other_data/basic_run_sheet.csv'
DATA_DIR='~/uy13/nsana4/result/K_Fold_Data'
K=5
TYPE='ph-ph'
COST=false

module load R/3.4.3
# for plotting
# uncomment below to run
# time R --vanilla < ~/uy13/nsana4/experiments/ignition_detection.R \
# --args $INPUT_FILE_DIR $DATA_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'p'

# Running logistic model
# uncomment below to run
# time R --vanilla < ~/uy13/nsana4/experiments/ignition_detection.R \
# --args $INPUT_FILE_DIR $DATA_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'm' $K $TYPE 'logistic' $COST

# Running other models : svm
# executed as sbatch --array=1-5 ignition_detection.sh
# uncomment below to run
time R --vanilla < ~/uy13/nsana4/experiments/ignition_detection.R \
--args $INPUT_FILE_DIR $DATA_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'm' $K $TYPE 'svm' ${SLURM_ARRAY_TASK_ID}

# Running other models : rf
# executed as sbatch --array=1-5 ignition_detection.sh
# uncomment below to run
# time R --vanilla < ~/uy13/nsana4/experiments/ignition_detection.R \
# --args $INPUT_FILE_DIR $DATA_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'm' $K $TYPE 'rf' ${SLURM_ARRAY_TASK_ID}

echo "Done"
