import::from(KernSmooth, locpoly)
import::from(plyr, ldply)
library(dplyr)


# Generates SD
# use_all_rows, when set to true, uses grid_size of number of rows in the data file in locpoly
# window_size, when greater than 0 uses the size window specified to compute summary (mean, sd, max etc)
# of the second derivative points
generate_second_derv <- function(base_dir,
                                 fault_file,
                                 result_dir,
                                 etype,
                                 use_all_rows = F,
                                 window_size = 0,
                                 summarise = T,
                                 save_result = T){
  read_fault_file <- function(etype){
    fault_file <- read.csv(fault_file,
                           check.names = F, stringsAsFactors = F) %>%
      filter(valid == 1 & type %in% etype) %>%
      mutate(file_name = sprintf("VT%03d.pnrf", test)) %>%
      data.frame(.)

    return(fault_file)
  }

  read_fault_file_data <- function(fault_file){
    dat <- data.frame(fname = list.files(path = base_dir, pattern="*.csv"))
    dat <- dat %>%
      mutate(id = gsub('.*(VT\\d{3,4}).*', '\\1', fname)) %>%
      filter(id %in% sprintf('VT%03d', fault_file$test))

    df <- apply(dat, 1, function(x){
      df <- read.csv(file.path(base_dir, x[['fname']]), stringsAsFactors = F) %>%
        select(time = 1, rms = 3) %>%
        mutate(id = x[['id']]) %>%
        data.frame(.)
    })
    df <- do.call('rbind', df)
    # df %>% select(id) %>% unique() %>% count()

    return(df)
  }

  compute_second_derivative <- function(dat){
    grid_size <- 401
    if(use_all_rows == T){
      grid_size <- nrow(dat)
    }
    fit1 <- locpoly(dat$time, dat$rms, drv=1, bandwidth = 0.25, gridsize = grid_size)
    fit2 <- locpoly(dat$time, dat$rms, drv=2, bandwidth = 0.25, gridsize = grid_size)

    return(data.frame(fit1.x = fit1$x, fit1.y = fit1$y,
                      fit2.x = fit2$x, fit2.y = fit2$y,
                      id = unique(dat$id)))
  }

  generate_window_seq <- function(dat){
    ws <- trunc(nrow(dat)/ window_size)
    ws_exact <- ws* window_size

    if(ws_exact == nrow(dat)){
      dat$wseq <- rep(1:ws, each = window_size)
    }
    else{
      if(ws == 0){
        dat$wseq <- rep(1, nrow(dat))
      }
      else{
        dat$wseq <- c(rep(1:ws, each = window_size),
                      rep(ws + 1, nrow(dat) - ws_exact))
      }
    }

    return(dat %>% data.frame())
  }

  generate_mov_window_summary <- function(dat){
    calculate_summary <- function(x, y, n){
      if(length(y) > window_size){
        y <- matrix(rep(y, each = window_size), ncol = window_size, byrow = T)

        # for moving avg calculation using vector
        y <- ldply(lapply(1:ncol(y), function(idx) {
          (c(rep(NA, (idx - 1)), y[,idx]))
        }), rbind)
        y <- y[window_size:(length(y) - window_size + 1)]
        x <- x[1:(length(x) - window_size + 1)]

        fit <- do.call(rbind, apply(y, 2, function(y1) {
          min_fit1 = min(y1, na.rm = T)
          max_fit1 = max(y1, na.rm = T)
          mean_fit1 = mean(y1, na.rm = T)
          quan_fit1 = quantile(y1, probs=c(.975), na.rm = T)
          sd_fit1 = sd(y1, na.rm = T)
          return(data.frame('1' = min_fit1,
                            '2' = max_fit1,
                            '3' = mean_fit1,
                            '4' = quan_fit1,
                            '5' = sd_fit1))
        }))
      }
      else{
        fit <- data.frame(
          min_fit1 = min(y, na.rm = T),
          max_fit1 = max(y, na.rm = T),
          mean_fit1 = mean(y, na.rm = T),
          quan_fit1 = quantile(y, probs=c(.975), na.rm = T),
          sd_fit1 = sd(y, na.rm = T))
        x <- x[1]
      }

      names(fit) <- c(paste0('min_fit_', n),
                      paste0('max_fit_', n),
                      paste0('mean_fit_', n),
                      paste0('quan_fit_', n),
                      paste0('sd_fit1_', n))
      fit$time <- x
      fit <- na.omit(fit)
      return(fit)
    }

    fit1 <- calculate_summary(dat$fit1.x, dat$fit1.y, 1)
    fit2 <- calculate_summary(dat$fit2.x, dat$fit2.y, 2)
    fit2$time <- NULL

    fit <- cbind(fit1, fit2)
    return(fit)
  }

  compute_sec_derv_summary <- function(dat, fault_file){
    if(summarise == T){
      if(window_size > 0){
        print(sprintf('summarising over window size %s', window_size))
        dat <- dat %>%
          group_by(id) %>%
          filter(n() > 30) %>%
          do(compute_second_derivative(.)) %>%
          group_by(id) %>%
          do(generate_mov_window_summary(.))

        # dat <- dat %>%
        #   group_by(id) %>%
        #   # compute over window size second derivative
        #   do(generate_window_seq(.)) %>%
        #   group_by(id, wseq) %>%
        #   summarise(min_fit1 = min(fit1.y, na.rm = T),
        #             max_fit1 = max(fit1.y, na.rm = T),
        #             mean_fit1 = mean(fit1.y, na.rm = T),
        #             quan_fit1 = quantile(fit1.y, probs=c(.975), na.rm = T),
        #             sd_fit1 = sd(fit1.y, na.rm = T),
        #             min_fit2 = min(fit2.y, na.rm = T),
        #             max_fit2 = max(fit2.y, na.rm = T),
        #             mean_fit2 = mean(fit2.y, na.rm = T),
        #             quan_fit2 = quantile(fit2.y, probs=c(.975), na.rm = T),
        #             sd_fit2 = sd(fit2.y)) %>%
        #   select(-wseq)
      }
      else{
        dat <- df %>%
          group_by(id) %>%
          filter(n() > 30) %>%
          do(compute_second_derivative(.)) %>%
          group_by(id) %>%
          summarise(min_fit1 = min(fit1.y, na.rm = T),
                    max_fit1 = max(fit1.y, na.rm = T),
                    mean_fit1 = mean(fit1.y, na.rm = T),
                    quan_fit1 = quantile(fit1.y, probs=c(.975), na.rm = T),
                    sd_fit1 = sd(fit1.y, na.rm = T),
                    min_fit2 = min(fit2.y, na.rm = T),
                    max_fit2 = max(fit2.y, na.rm = T),
                    mean_fit2 = mean(fit2.y, na.rm = T),
                    quan_fit2 = quantile(fit2.y, probs=c(.975), na.rm = T),
                    sd_fit2 = sd(fit2.y, na.rm = T))
      }

      # add ignition label
      temp1 <- fault_file %>%
        mutate(id = sprintf('VT%03d', test)) %>%
        select(id, ignition, species, type)
      dat <- merge(dat, temp1, by = 'id')
      dat$ignition <- factor(as.character(dat$ignition), level = c('1', '0'))
      if(save_result == T){
        write.csv(dat, file.path(result_dir, 'second_derivative_summary.csv'), row.names = F)
      }
    }
    else{
      print('Not summarising data...')
      dat <- dat %>%
        group_by(id) %>%
        filter(n() >= 30) %>%
        do(compute_second_derivative(.))

      # add ignition label
      temp1 <- fault_file %>%
        mutate(id = sprintf('VT%03d', test)) %>%
        select(id, ignition, species, type)
      dat <- merge(dat, temp1, by = 'id')
      dat$ignition <- factor(as.character(dat$ignition), level = c('1', '0'))
      if(save_result == T){
        write.csv(dat, file.path(result_dir, 'second_derivative.csv'), row.names = F)
      }
    }


    return(dat)
  }

  # read fault file
  # type: ph-ph, ph-e
  fault_file <- read_fault_file(etype)

  # read the downsampled data as a data frame
  dat <- read_fault_file_data(fault_file)
  dat <- compute_sec_derv_summary(dat, fault_file)

  return(dat)
}


