#!/bin/env bash
#SBATCH --job-name=SD_Threshold-%j-%A
#SBATCH --time=3:00:00
#SBATCH --mem=16000mb
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=nandini.anantharama@monash.edu

echo "Starting on `hostname`"
echo "JOBID is  $SLURM_JOB_ID  and SLURM_ARRAY_TASK_ID is  ${SLURM_ARRAY_TASK_ID}"

# input directory for porcessing files
INPUT_FILE_DIR='~/uy13/DataFolder/From_Start_To_Max/'
OUTPUT_FILE_DIR='~/uy13/nsana4/result/'
FAULT_FILE_LOC='~/uy13/DataFolder/Other_data/basic_run_sheet.csv'
K=5
COST=false

echo "Processing directory corresponding to $SLURM_ARRAY_TASK_ID is $INPUT_FILE_DIR"
echo "Calling R module with $INPUT_FILE_DIR, $OUTPUT_FILE_DIR, $FAULT_FILE_LOC  as file args"

module load R/3.4.3
echo "Calling sd_threshold.R for plotting"
time R --vanilla < ~/uy13/nsana4/experiments/sd_threshold.R --args $INPUT_FILE_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'p'
echo "Calling sd_threshold.R for modeling"
time R --vanilla < ~/uy13/nsana4/experiments/sd_threshold.R --args $INPUT_FILE_DIR $OUTPUT_FILE_DIR $FAULT_FILE_LOC 'm' $K $COST
echo "Done"
