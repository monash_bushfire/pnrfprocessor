#!/bin/env bash
#SBATCH --job-name=TS_Features-%j-%A
#SBATCH --time=3:00:00
#SBATCH --mem=16000mb
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=nandini.anantharama@monash.edu

echo "Starting on `hostname`"
echo "JOBID is  $SLURM_JOB_ID  and SLURM_ARRAY_TASK_ID is  ${SLURM_ARRAY_TASK_ID}"

FILENAME=`find ~/uy13/DataFolder/DownSampled_Data/ -maxdepth 1 -name *.csv -type f -printf '%f\n' | head -${SLURM_ARRAY_TASK_ID} | tail -1`
echo "Filename corresponding to $SLURM_ARRAY_TASK_ID is $FILENAME"
echo "Calling R module with $FILENAME as args"

module load R/3.4.3
time R --vanilla < ~/uy13/nsana4/ts_features.R --args $FILENAME
echo "Done"
