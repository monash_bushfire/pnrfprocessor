library(dplyr)
import::from(splitstackshape, stratified)

# loads the function and generates k-fold cross validation data
# this is necessary so as to run svm model in parallel
generate_data <- function(experiments.folder,
                          experiments.folder.fault_file,
                          experiments.folder.result,
                          k,
                          etype,
                          equal) {
  # get fault file for filtering valid files (species if required)
  get_fault_file <- function() {
    fault_file <- read.csv(experiments.folder.fault_file) %>%
      filter(valid == 1 &
               (ignition %in% c(0, 1)) & type %in% c('ph-ph', 'ph-e')) %>%
      data.frame(.)
    fault_file <- na.omit(fault_file)
    return(fault_file)
  }

  read_data <- function() {
    dat <- list.files(path = experiments.folder, pattern = "*.csv")
    df <- lapply(dat, function(x) {
      df <- read.csv(file.path(experiments.folder, x))
      df$id <- gsub('*(VT\\d{2,4})*.', '\\1', x)
      df
    })
    df <- do.call('rbind', df)
    fault_file <- get_fault_file()
    fault_file$id <- sprintf('VT%03d', fault_file$test)

    df <-
      merge(df, fault_file[, c('id', 'species', 'type', 'ignition')])
    df$ignition <-
      factor(as.character(df$ignition), levels = c('1', '0'))

    return(df)
  }

  # read feature.csv files from the folder location, and filter for files that are
  # marked as valid in the basic_signature_file (fault_file)
  # change from read data to read_data_with_igntion_split for 50-50 split percentate
  read_data_with_ignition_split <- function() {
    dat <- list.files(path = experiments.folder, pattern = "*.csv")
    fault_file <- get_fault_file()
    fault_file$id <- sprintf('VT%03d', fault_file$test)

    df <- lapply(dat, function(x) {
      df <- read.csv(file.path(experiments.folder, x))
      df <- df %>%
        mutate(
          id = gsub('*(VT\\d{2,4})*.', '\\1', x),
          temp_ignition = case_when(row_number() <= trunc(nrow(.) /
                                                            2) ~ 0,
                                    TRUE ~ 1)
        ) %>%
        data.frame(.)
      # df$id <- gsub('*(VT\\d{2,4})*.', '\\1', x)
    })
    df <- do.call('rbind', df)
    df <- merge(df,
                fault_file[, c('id', 'species', 'type', 'ignition')],
                by = c('id'))
    # set half the file to 0, 1 when case is 1
    df <- df %>%
      mutate(ignition = case_when(ignition == 1 &
                                    temp_ignition == 1 ~ 1,
                                  TRUE ~ 0)) %>%
      select(-temp_ignition) %>%
      data.frame(.)
    df$ignition <-
      factor(as.character(df$ignition), levels = c('1', '0'))


    df <- na.omit(df)

    return(df)
  }

  # splits sample into type + igntion
  get_k_fold_all_labels <- function(dat) {
    ids <- dat %>%
      select(id, ignition, type) %>%
      unique(.)

    t <- ids %>%
      group_by(ignition, type) %>%
      summarise(n = n()) %>%
      ungroup %>%
      mutate(per = n / sum(n))

    ids$labels <- paste(ids$ignition, ids$type, sep = '_')

    k_folds_label1_ph_e <- trunc(t[1, ]$n / k)
    k_folds_label1_ph_ph <- trunc(t[2, ]$n / k)
    k_folds_label0_ph_e <- trunc(t[3, ]$n / k)
    k_folds_label0_ph_ph <- trunc(t[4, ]$n / k)

    train_set <- list()
    for (i in c(1:(k - 1))) {
      set.seed(19102018)
      train <- stratified(
        ids,
        c('labels'),
        c(
          '1_ph-e' = k_folds_label1_ph_e,
          '1_ph-ph' = k_folds_label1_ph_ph,
          '0_ph-e' = k_folds_label0_ph_e,
          '0_ph-ph' = k_folds_label0_ph_ph
        ),

        replace = F
      )
      train_set[[i]] <- dat[dat$id %in% train$id,]
      dat <- dat[!dat$id %in% train$id, ]
      ids <- dat %>%
        select(id, ignition) %>%
        unique(.)
    }
    train_set[[k]] <- dat

    return(train_set)
  }

  get_k_fold <- function(dat) {
    ids <- dat %>%
      select(id, ignition) %>%
      unique(.)

    t <- ids %>%
      group_by(ignition) %>%
      summarise(n = n()) %>%
      ungroup %>%
      mutate(per = n / sum(n))

    # depends on levels, c(1, 0)
    k_folds_label1 <- trunc(t[1, ]$n / k)
    k_folds_label0 <- trunc(t[2, ]$n / k)
    train_set <- list()

    if (equal == T) {
      print('generating equal samples...')
      # sample 1 and 0s in equal
      k_fold_label <- max(k_folds_label0, k_folds_label1)

      for (i in c(1:k)) {
        train <- stratified(ids,
                            c('ignition'),
                            c('1' = k_fold_label,
                              '0' = k_fold_label),
                            replace = T)
        train_set[[i]] <- dat[dat$id %in% train$id,]
      }
    }
    else{
      print('generating stratified samples...')
      for (i in c(1:(k - 1))) {
        set.seed(19102018)
        train <- stratified(ids,
                            c('ignition'),
                            c('1' = k_folds_label1,
                              '0' = k_folds_label0),
                            replace = F)
        train_set[[i]] <- dat[dat$id %in% train$id,]
        dat <- dat[!dat$id %in% train$id, ]
        ids <- dat %>%
          select(id, ignition) %>%
          unique(.)
      }

      train_set[[k]] <- dat
    }

    return(train_set)
  }

  generate_folds <- function(train_set, type) {
    k_fold_data_list <- list()
    for (i in c(1:k)) {
      train_i <- setdiff(c(1:k), i)
      k_fold_data <-
        list(
          train = do.call('rbind', train_set[train_i]),
          test = train_set[[i]],
          k_fold = i,
          type = type
        )

      # print(sprintf('saving %s fold data', i))
      # save(k_fold_data,
      #      file = file.path(
      #        experiments.folder.result,
      #        sprintf('ts_features_%s_%s_fold.rda', type, i)
      #      ))

      k_fold_data_list[[i]] <- k_fold_data
    }

    return(k_fold_data_list)
  }

  # execution begins here
  # read the data file
  dat <- read_data()

  # Run k-fold only if k> 1
  print('dat')
  print(nrow(dat))
  print('k-fold')
  print(k)

  if (!is.null(dat) & nrow(dat) > k & k > 1) {
    print('generating folds...')

    k_fold_data_list <- list()
    if(etype == 'all'){
      train_set <- get_k_fold(dat)
      k_fold_data_list[['all']] <- generate_folds(train_set = train_set, 'all')
    }
    else{
      train_set <- get_k_fold(dat %>% filter(type == 'ph-ph'))
      k_fold_data_list[['ph-ph']] <- generate_folds(train_set = train_set, 'ph-ph')

      train_set <- get_k_fold(dat %>% filter(type == 'ph-e'))
      k_fold_data_list[['ph-e']] <- generate_folds(train_set = train_set, 'ph-e')
    }

    return(k_fold_data_list)
  }

  return(dat)
}
